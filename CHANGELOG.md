v5.6.0
- fix #80 now correct wind speeds are displayed with current values (before daily values)
- fix #81 small redesign for visual separation of current data and day data
- fix #82 implement new forecast rain icons for hourly and daily forecast (thanks @cibersheep for the icons)
- add current cloud coverage value
- with m/s as unit for wind speed, now 1 decimal is given
- improve vertical alignment of daily forecast information
- translation updates, many thanks to all translators!

v5.5.3
- fix some error messages with upcoming Qt5.12 (thanks @dobey)
- Translation updates, many thanks to all translators!

v5.5.2
- smaller UI changes to current weather to improve handling of long condition texts
- fixing some translations not using abbreviated units
- Translation updates, many thanks to all translators!

v5.5.1
- fix feels like string not being translated

v5.5.0
- add "feels like" temperature to current info (if available at your location)
- add rain volume to hourly data and day extended info section
- add setting for rain volume unit
- add m/s as unit for wind speed (official SI unit)
- restructure internal unit handling and default unit values
- replace kph with m/s as default wind speed unit unit, fix #76

v5.4.0
- locations and location search now uses localized city names (if availabe at geonames.org), fix #73. Existing locations need to be removed and added again to use the localized name.
- predefined list of cities now contains cities with more than 5 million inhabitants (according to geonames.org data), fix #74
- redesign of location search to always show search bar
- improve animation when changing locations from indicator or location list, fix #46
- Translation updates, many thanks to all translators!

v5.3.0
- add about page including credits
- remapped icons for cloud and rain to show more separate conditions
- fix #64 mismatch of icon and text for current weather condition
- fix #72 wrong sunrise and sunset times in places with daylightsaving
- Translation updates, many thanks to all translators!

v5.2.2
- Translation updates, many thanks to all translators!

v5.2.1
- remove color select option for highlight today, provide grey or none for highlighting

v5.2.0
- add option to set background color for todays weather info, fix #59
- show pressure in extended information section
- remove setting to toggle pressure information
- remove dividers for a consistent appwide appearance

v5.1.5
- Translation updates, many thanks to all translators!

v5.1.3
- Translation updates, many thanks to all translators!

v5.1.2
- use themed colors for blue and red
- some small UI changes to rain radar icon and header
- Translation updates, many thanks to all translators!

v.5.1.1
- fix settings page not showing correct version number
- new icons for rain radar map to make usage more intuitive

v.5.1.0
- add weather radar provided by rainviewer.com (many thanks!)
- fix button margins in networkError page

v5.0.6
- fix #66 sunrise and sunset times off by one hour in areas with daylight saving
- Translation updates, many thanks to all translators!

v5.0.5
- temperature color setting shows the current selected value as subtitle
- Translation updates, many thanks to all translators!

v5.0.4
- fix #63 theme not applied to non colored temperatures
- Translation updates, many thanks to all translators!

v5.0.3
- Translation updates, many thanks to all translators!

v5.0.2
- exchange moon emojis with svg-graphics, many thanks to @cibersheep for the artwork!

v5.0.1
- reduze size of dots to match app scopes
- add blue/yellow color theme for high/low temperatures
- adjust spacing between daily and hourly forecast, fix #61
- Translation updates, many thanks to all translators!

v5.0.0
- finish weather app redesign #44
  * add page indicator if more than one location is set
  * add up/down icon to day details
  * hourly forecast always visible
  * make colors more consistent, fix #54
- fix spacing/height of moonphase text, moon emoji infront of description, fix #55,
- fix bottom edge not working on theme change #56
- fix bad alignment on daily forecast and locations
- add offline mode and network available indicator, fix #57
- add pressure data values
- add country name to location in location list
- allow refresh intervals of up to 4 hours
- add info page for "hidden" functions in locations page
- settings page only flickable if needed
- Translation updates, many thanks to all translators!

v4.5.2
- Translation updates, many thanks to all translators!

v4.5.1
- fix small visibility issue in settings page

v4.5.0
- add icon for expanding/collapsing daily details page
- fix #51: add option to use system theme
- fix #53: sunset/sunrise times now shown in their local time (thanks Lorenzo)
- add wordwrap for long moonphase strings due to translation

v.4.4.2
- Translation updates, many thanks to all translators!

v4.4.1
- small internal tweak

v.4.4.0
- update suncalc.js from its repo
- small improvements to moonphase calculations
- add moonphase emojis for visualization
- omit 0% humidity values (forecast only available for 3 days)

v4.3.1
- Fixed missing api key

v4.3.0
- corrected sunrise and sunset times calculation (fix #48)
- moonphase information added

v4.2.0
- Weekday and calendar date show in week view

v4.1.0
- Redesigned setting page
- New dark mode toggle

v4.0.0
- Redesigned icons
- Dark theme support
- The app can now be rotated to a landscape view
- Improved splash screen

v3.6.2 & v3.6.3 & v3.6.4 & v3.6.5 & v3.6.6
- Translation update, thank you translators!

v3.6.1
- Temporarily removed "The Weather Channel" data provider. UBports only has an api key for OpenWeatherMap at this time. We apologize for the inconvenience. We hope to add the option to input your own api key for different services in the future.

v3.6.0
- Translation update
