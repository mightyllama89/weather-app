/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import "../components"
import "../data/keys.js" as Keys


Page {
    // Set to null otherwise the header is shown (but blank) over the top of the listview
    id: locationPage
    objectName: "homePage"
    flickable: null

    Rectangle {
      //when swiping BottomEdge the DayDelegate underneath gets clicked
      //transparent MouseArea to catch click events near BottomEdge
      id: avoidDayDelegateClick
      width: parent.width
      height: units.gu(4)
      color: "transparent"
      z:1 //raise above DayDelegate's
      anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
      }
      MouseArea {
          anchors.fill: parent
          cursorShape: Qt.PointingHandCursor
          propagateComposedEvents: false
          hoverEnabled: false
          onClicked: { bottomEdge.commit()}
      }
    }

    BottomEdge {
        id: bottomEdge
        height: parent.height
        preloadContent: false //needs to be set to false to allow theme changing without errors

        // Note: cannot use contentUrl and preload until pad.lv/1604509
        contentComponent: locationsPage

        Component {
            id: locationsPage
            LocationsPage {
                height: bottomEdge.height
                width: bottomEdge.width

                onPop: bottomEdge.collapse()
                onThemeChanged: {
                        //HACK: Dirty hack to avoid bottomEdge stops working after changing themes
                        //preloadContent needs to be set to false for this to work
                        bottomEdge.commit()
                        bottomEdge.collapse()
                    }
            }
        }
        hint.text: bottomEdge.hint.status == BottomEdgeHint.Locked ? i18n.tr("Locations") : ""
    }

    //system weather icons are used for forecast icons (3h and daily)
    property var iconMap: {
        "sun": "weather-clear-symbolic",
        "moon": "weather-clear-night-symbolic",
        "few_cloud_sun": "weather-few-clouds-symbolic",
        "few_cloud_moon": "weather-few-clouds-night-symbolic",
        "scattered_cloud": "weather-clouds-symbolic",
        "cloud": "weather-overcast-symbolic",
        "showers": "weather-showers-scattered-symbolic",
        "rain": "weather-showers-symbolic", //no rain icon available (??) -> maybe rename icon upstream?
        "thunder": "weather-storm-symbolic",
        "snow": "weather-snow-symbolic",
        "fog": "weather-fog-symbolic"
    }
//other available weather icons currently unused
// weather-flurries-symbolic
// weather-sleet-symbolic
// weather-hazy-symbolic
// weather-severe-alert-symbolic

    //local stored svg's are used for todays condition icon
    //"normal" icons lines are too thick when scaled up that big
    property var imageMap: {
        "sun": Qt.resolvedUrl("../graphics/weather-clear.svg"),
        "moon": Qt.resolvedUrl("../graphics/weather-clear-night.svg"),
        "few_cloud_sun": Qt.resolvedUrl("../graphics/weather-few-clouds.svg"),
        "few_cloud_moon": Qt.resolvedUrl("../graphics/weather-few-clouds-night.svg"),
        "scattered_cloud": Qt.resolvedUrl("../graphics/weather-clouds.svg"),
        "cloud": Qt.resolvedUrl("../graphics/weather-overcast.svg"),
        "showers": Qt.resolvedUrl("../graphics/weather-showers-scattered.svg"),
        "rain": Qt.resolvedUrl("../graphics/weather-showers.svg"),
        "thunder": Qt.resolvedUrl("../graphics/weather-storm.svg"),
        "snow": Qt.resolvedUrl("../graphics/weather-snow.svg"),
        "fog": Qt.resolvedUrl("../graphics/weather-fog.svg"),
    }

    /*
     wrapper function to extract wind bearings delivered from API and return them as translatable strings
    */
    function getWindBearing(windBearing) {

        if (windBearing == "N") {
            // TRANSLATORS: N = North, wind bearing, abbreviated
            return i18n.tr("N");
        } else if (windBearing == "NE") {
            // TRANSLATORS: NE = North East, wind bearing, abbreviated
            return i18n.tr("NE");
        } else if (windBearing == "E") {
            // TRANSLATORS: E = East, wind bearing, abbreviated
            return i18n.tr("E");
        } else if (windBearing == "SE") {
            // TRANSLATORS: SE = South East, wind bearing, abbreviated
            return i18n.tr("SE");
        } else if (windBearing == "S") {
            // TRANSLATORS: S = South, wind bearing, abbreviated
            return i18n.tr("S");
        } else if (windBearing == "SW") {
            // TRANSLATORS: SW = South West, wind bearing, abbreviated
            return i18n.tr("SW");
        } else if (windBearing == "W") {
            // TRANSLATORS: W = West, wind bearing, abbreviated
            return i18n.tr("W");
        } else if (windBearing == "NW") {
            // TRANSLATORS: NW = NorthWest, wind bearing, abbreviated
            return i18n.tr("NW");
        } else {
            //if wind bearing is none of the above just push it trough without translation
            return windBearing;
        }
    }

    /*
      Format date object by given format.
    */
    function formatTimestamp(dateData, format) {
        return Qt.formatDate(getDate(dateData), i18n.tr(format))
    }

    /*
      Format date object by given format.
    */
    function formatTimestampLocaleShort(dateData) {
        return Qt.formatDate(getDate(dateData))
    }

    /*
      Format time object by given format.
    */
    function formatTime(dateData, format) {
        return Qt.formatTime(getDate(dateData), i18n.tr(format))
    }

    /*
      Get Date object from dateData including hour data.
    */
    function getDate(dateData) {
        return new Date(dateData.year, dateData.month, dateData.date, dateData.hours, dateData.minutes)
    }

    /*
      Get Date object from dateData with additional offset [in minutes].
    */
    function getCustomDate(dateData,offsetMin) {
        return new Date(dateData.year, dateData.month, dateData.date, dateData.hours, dateData.minutes + offsetMin)
    }

    function emptyIfUndefined(variable, append) {
        if (append === undefined) {
            append = ""
        }
        return variable === undefined ? "" : variable + append
    }

    //add temperature unit and convert if neccessary
    function getTemp(tempToConvert, showunit) {
        var value = tempToConvert
        //query settings here, so function can be called without unit, used for current temperature
        if (settings.tempScale === "°F") {
            value = Math.round(calcFahrenheit(value)).toString()
        } else {
            value = Math.round(value).toString()
        }
        if (showunit) {
            value = value + i18n.tr(settings.tempScale)
        }
        return value;
    }

    //add wind speed unit and convert if neccessary
    function getWindSpeed(speed) {
        var value = speed //raw wind speed in m/s
        if (settings.windUnits === "m/s") {
            value = Math.round(value*10)/10; // keep one decimal
        } else if (settings.windUnits === "km/h") {
            value = Math.round(calcKmh(value));
        } else if (settings.windUnits === "mph") {
            value = Math.round(calcMph(value));
        } else {
            console.log("wind unit error ")
        }
        return value.toString() + " " + i18n.tr(settings.windUnits);
    }

    //add rain volume unit, adjust decimals and convert if neccessary
    function getRainSnowValue(rainsnow) {
        var value = rainsnow
        // convert to inch if needed and round numbers
        if (settings.precipUnits === "in") {
            // keep two dezimals
            value = Math.round(calcInch(value)*100)/100;
        } else {
            // keep one dezimal
            value =  Math.round(value*10)/10
        }
        // add unit according to setting
        // convert from mm to in if neccessary
        // put in decimal separator according to set locale
        if (settings.precipUnits == "in") {
            value = value.toFixed(2).toString().replace(".",Qt.locale().decimalPoint) + " " + i18n.tr("in")
        } else {
            value = value.toString().replace(".",Qt.locale().decimalPoint) + " " + i18n.tr("mm")
        }
        return value;
    }
    /*
    unit converting helper functions
    */
    //convert mm to in, both volume per hour
    function calcInch(mm) {
        return mm/25.4;
    }
    //convert celsius to fahrenheit
    function calcFahrenheit(celsius) {
            return celsius * 1.8 + 32;
    }
    // convert m/s to mph
    function calcMph(ms) {
        return ms*2.24;
    }

    //convert
    function calcKmh(ms) {
        return ms*3.6;
    }
    //
    function convertKmhToMph(kmh) {
        return kmh*0.621;
    }


    // Do not show the Page Header
    header: PageHeader {
        visible: false
    }

    /*
      ListView for locations with snap-scrolling horizontally.
    */
    ListView {
        id: locationPages
        objectName: "locationPages"
        anchors {
            fill: parent
        }
        contentHeight: parent.height - bottomEdge.hint.height
        currentIndex: settings.current
        delegate: LocationPane {
            objectName: "locationPane" + index
        }
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 50
        highlightMoveVelocity: 600
        model: weatherApp.locationsList.length
        orientation: ListView.Horizontal
        // TODO with snapMode, currentIndex is not properly set and setting currentIndex fails
        //snapMode: ListView.SnapOneItem

        property bool loaded: false

        signal collapseOtherDelegates(int index)

        onCurrentIndexChanged: {
            if (loaded) {
                // FIXME: when a model is reloaded this causes the currentIndex to be lost
                settings.current = currentIndex

                collapseOtherDelegates(-1)  // collapse all
            }
        }
        onModelChanged: {
            currentIndex = settings.current

            if (model > 0) {
                loading = false
                loaded = true
            }
        }
        onVisibleChanged: {
            if (!visible && loaded) {
                collapseOtherDelegates(-1)  // collapse all
            }
        }

        // TODO: workaround for not being able to use snapMode property
        Component.onCompleted: {
            var scaleFactor = units.gridUnit * 10;
            maximumFlickVelocity = maximumFlickVelocity * scaleFactor;
            flickDeceleration = flickDeceleration * scaleFactor;
        }

        Connections {
            target: settings
            onCurrentChanged: {
                locationPages.currentIndex = settings.current
            }
        }
    }

    LoadingIndicator {
        id: loadingIndicator
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        processing: loading
    }

    Loader {
        active: (locationsList === null || locationsList.length === 0) && mainPageStack.depth === 1
        anchors {
            fill: parent
        }
        asynchronous: true
        source: "../components/HomePageEmptyStateComponent.qml"
        visible: status === Loader.Ready && active
    }

    Loader {
        active: mainPageStack.depth === 1 && !Keys.twcKey && !Keys.owmKey
        anchors {
            fill: parent
        }
        asynchronous: true
        source: "../components/NoAPIKeyErrorStateComponent.qml"
        visible: status === Loader.Ready && active
    }
}
