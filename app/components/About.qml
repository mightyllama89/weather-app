/*
 * Copyright (C) 2020 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: aboutPage

    header: PageHeader {
        id: header
        title: i18n.tr("About")
    }

    Flickable {
        id: aboutFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            topMargin: aboutPage.header.height + units.gu(1)
            fill: parent
        }

        contentHeight: aboutColumn.childrenRect.height

        Column {
            id: aboutColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Item {
                id: appIcon
                width: parent.width
                height: app_icon.height + units.gu(4)

                UbuntuShape {
                    id: app_icon

                    width: Math.min(aboutPage.width/4, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("../weather-app.svg")
                    }
                    radius: "medium"
                    aspect: UbuntuShape.DropShadow
                }
            }

            ListItem {
                id: appLabel
                height: appLayout.height
                divider { visible: false; }
                ListItemLayout {
                    id: appLayout
                    title.text: i18n.tr("Weather app")
                    title.font.pixelSize: units.gu(2.5)
                    title.horizontalAlignment: Text.AlignHCenter
                    subtitle.text: "v%1".arg(Qt.application.version)
                    subtitle.font.pixelSize: units.gu(1.75)
                    subtitle.horizontalAlignment: Text.AlignHCenter
                }
            }

            Repeater {
                id: listViewAbout

                model: [
                { name: i18n.tr("Get the sourcecode"), url: "https://gitlab.com/ubports/apps/weather-app/" },
                { name: i18n.tr("Report issues"),  url: "https://gitlab.com/ubports/apps/weather-app/-/issues" },
                { name: i18n.tr("Help translate"), url: "https://translate.ubports.com/projects/ubports/weather-app/" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot { }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Rectangle {
                id: spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Label {
                id: headlineLabel
                text: i18n.tr("Credits")
                font.pixelSize: units.gu(1.8)
                font.bold: true
                font.underline: true
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                }
            }

            Repeater {
                id: listViewCredits

                model: [
                    {
                        name: i18n.tr("Weather data by") + " openweathermap.org",
                        // licence: i18n.tr("Licence: commercial data"),
                        // summary: i18n.tr("Provides weather data via API"),
                        url: "https://openweathermap.org/"
                    },
                    {
                        name: i18n.tr("Rainradar by") + " Rainviewer.com",
                        // licence: i18n.tr("Licence: free, depending on data source"),
                        // summary: i18n.tr("Provides a collection of weather data from free sources"),
                        url: "https://www.rainviewer.com/"
                    },
                    {
                        name: i18n.tr("Coordinate lookup by") + " geoip.ubuntu.com",
                        // licence: i18n.tr("Licence: Canonical terms and conditions"),
                        // summary: i18n.tr("Allows to retreive coordinates for a given location name"),
                        url: "https://wiki.ubuntu.com/mapuntu/API"
                    },
                    {
                        name: i18n.tr("Location data lookup by") + " geonames.org",
                        // licence: i18n.tr("Licence: CC BY 4.0"),
                        // summary: i18n.tr("Allows to retreive administrative data e.g. country name for a given location"),
                        url: "http://www.geonames.org/"
                    },
                    {
                        name: i18n.tr("Sun and moon calculations by") + " suncalc.js",
                        // licence: i18n.tr("Licence: BSD"),
                        // summary: i18n.tr("Javascript library for calculating sun and moon related data"),
                        url: "https://github.com/mourner/suncalc"
                    },
                    {
                        name: i18n.tr("Timezone converting by") + " moment.js",
                        // licence: i18n.tr("Licence: MIT"),
                        // summary: i18n.tr("Moment with MomentTimezone is using build-in IANA timezone data"),
                        url: "https://momentjs.com/"
                    }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutCredits.height
                    ListItemLayout {
                        id: layoutCredits
                        title.text : modelData.name
                        // subtitle.text : modelData.licence
                        // summary.text: modelData.summary
                        // summary.wrapMode: Text.WordWrap
                        ProgressionSlot { }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }
        }
    }
}
