# Weather app

Weather app does provide current weather information and a weather forecast for your mobile device running [Ubuntu Touch](https://ubuntu-touch.io/).

<img src="screenshot_readme.png" alt="screenshot" width="182"/>

&nbsp;

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/com.ubuntu.weather)

## Ubuntu Touch

This app is a core app for [Ubuntu Touch](https://ubuntu-touch.io/).
Ubuntu Touch is a mobile OS developed by [UBports](https://ubports.com/).
A group of volunteers and passionate people across the world.
With Ubuntu Touch we offer a truly unique mobile experience - an alternative to the current most popular operating systems on the market.
We believe that everyone is free to use, study, share and improve all software created by the foundation without restrictions.
Whenever possible, everything is distributed under free and open source licenses endorsed by the Free Software Foundation, the Open Source Initiative.

## Importing locations via URL

Weather app is capable of importing locations via a custom url scheme. Locations can be directly imported using a link like this:

[weather://?city=London&lat=51.50853&lng=-0.12574](weather://?city=London&lat=51.50853&lng=-0.12574)

OpenWeatherMap search query links can be submitted too. In this case the first search result will be added as location. Search query links need to be specified like this:

[https://openweathermap.org/find?q=london](https://openweathermap.org/find?q=london)

The first option is mainly useful for access from within other apps using a command like this: `Qt.openUrlExternally("weather://?city=London&lat=51.50853&lng=-0.12574")`. The second type of link can be used the same or simply copied into Teleports and clicked there for import.

By simply calling [weather://](weather://) without any parameters weather app is opened.

*Note: This feature in its current basic implementation is not (yet) meant for end users but for app developers to get some access to weather app.*

## Reporting Bugs and Requesting Features

Bugs and feature requests can be reported via our [bug tracker](https://gitlab.com/ubports/apps/weather-app/issues).

## Translating

This app can be translated on the [UBports Weblate](https://translate.ubports.com/projects/ubports/weather-app/).
We welcome translators from all different languages. Thank you for your contribution!

To add a new language, log into weblate, goto *tools* --> *start new translation*.

## Developing

1. Install [Clickable](http://clickable.bhdouglass.com/en/latest/)

2. Fork [weather app at gitlab](https://gitlab.com/ubports/apps/weather-app) into your own namespace. You may need to open an account with gitlab if you do not already have one.

3. Open a terminal by pressing: `ctl + alt + t` (ubuntu).

4. Clone the repo onto your local machine using git:

    `git clone https://gitlab.com/ubports/apps/weather-app.git`.

5. Change into weather apps folder: `cd weather-app`.

6. Acquire an API key from [OpenWeatherMap](https://openweathermap.org/).

This app currently only supports weather data from OpenWeatherMap. Visit [OpenWeatherMap's API page](https://openweathermap.org/api) for detailed information about the API and how to obtain a personal key. Get a key and save it as the `owmKey` variable in `app/data/keys.js`, as seen here:

```javascript
var owmKey = "<your_key_here>";
```

**Do not commit branches with private keys in place! A centrally managed key is injected at build time.**

7. Run `clickable` from the root of this repository to build and deploy weather-app on your attached Ubuntu Touch device, or run `clickable desktop` to test weather app on your desktop. You can run `clickable log` for debugging information.

*Note: On desktop machines the app's network check does not work. Therefore, when using `clickable desktop` for developing and testing, in `app/ubuntu-weather-app.qml` the following three lines need to be changed temporary:*

```
line  67: property bool networkError: false
line  73: networkError = false
line 215: networkError = false
```

### Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, Matrix ( #ubcd:matrix.org ), or any other method with the owners of this repository before making a change.
Please note we have a [code of conduct](https://ubports.com/foundation/ubports-foundation/foundation-codeofconduct), please follow it in all your interactions with the project.

*Don't push tags to your repo. This would trigger the gitlab CI to automatically build a new release and publish it to OpenStore. This process would result in a error due to wrong API key anyway.*

### Maintainer

This Ubuntu Touch core app is being maintained by [Daniel Frost](https://gitlab.com/Danfro).

See also the list of [contributors](https://gitlab.com/ubports/apps/weather-app/-/graphs/master) who participated in this project.

### Releasing

New releases are automatically pushed to the OpenStore via the GitLab CI. To make a new release a maintainer must:

- Increment the version in the manifest.json
- Create a new git commit, the commit message will become part of the automatic changelog in the OpenStore
- Tag the commit with a tag starting with `v` (for example: `git tag v1.2.3`)
- Push the new commit and tag to GitLab: `git push && git push --tags`
- The GitLab CI will build and publish the new release to the OpenStore

### Useful Links

- [UBports App Dev Docs](http://docs.ubports.com/en/latest/appdev/index.html)
- [UBports SDK Docs](https://api-docs.ubports.com/)
- [UBports](https://ubports.com/)
- [Ubuntu Touch](https://ubuntu-touch.io/)
- [OpenWeatherMap API](https://openweathermap.org/api)

## Donating

If you love UBports and it's apps please consider dontating to the [UBports Foundation](https://ubports.com/donate). Thanks in advance for your generous donations.

## License

This app is licensed under the GNU GPL v3 license.

Weather App is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.
Weather App is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  
If not, see [www.gnu.org/licenses](http://www.gnu.org/licenses/).
